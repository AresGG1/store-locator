<?php

declare(strict_types=1);

namespace Course\StoreLocator\Ui\DataProvider\Category\Listing;

use Magento\Framework\View\Element\UiComponent\DataProvider\SearchResult;

class Collection extends SearchResult
{
    /**
     * Override _initSelect to add custom columns
     *
     * @return void
     */
    protected function _initSelect()
    {
//        $this->addFilterToMap('id', 'course_football.id');
//        $this->addFilterToMap('name', 'name.value');
        parent::_initSelect();
    }
}