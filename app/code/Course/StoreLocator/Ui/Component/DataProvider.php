<?php

declare(strict_types=1);

namespace Course\StoreLocator\Ui\Component;

use Course\StoreLocator\Model\ResourceModel\Store\CollectionFactory;
use Magento\Store\Model\StoreManager;

class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    private StoreManager $storeManager;

    /**
     * @param CollectionFactory $collectionFactory;
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        StoreManager $storeManager,
        CollectionFactory $collectionFactory,
        string $name,
        string $primaryFieldName,
        string $requestFieldName,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $collectionFactory->create();
        $this->storeManager = $storeManager;
    }

    /**
     * Get data
     *
     * @return array
     */

    private $loadedData;

    public function getData(): ?array
    {
        $items = $this->collection->getItems();
        foreach ($items as $model) {
            $this->loadedData[$model->getId()] = $model->getData();
            if ($model->getImage()) {
                // replace icon to your custom field name
                $m['image'][0]['name'] = $model->getImage();
                $m['image'][0]['type'] = 'image';
                $m['image'][0]['url'] = $this->getMediaUrl().$model->getImage();
                $fullData = $this->loadedData;
                $this->loadedData[$model->getId()] = array_merge($fullData[$model->getId()], $m);
            }
        }
        return $this->loadedData;
    }
    public function getMediaUrl()
    {
        $mediaUrl = $this->storeManager->getStore()
                ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        return $mediaUrl;
    }
}