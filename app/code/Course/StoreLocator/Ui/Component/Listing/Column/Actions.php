<?php

declare(strict_types=1);

namespace Course\StoreLocator\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

/**
 * Class Actions
 */
class Actions extends Column
{
    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */

    private const EDIT_PATH = 'store_locator/index/edit/';
    private const DELETE_PATH = 'store_locator/index/delete/';
    private const DUPLICATE_PATH = 'store_locator/index/duplicate/';
    private \Magento\Framework\UrlInterface $urlBuilder;

    public function __construct(ContextInterface $context, UiComponentFactory $uiComponentFactory, array $components = [], array $data = [],
                                \Magento\Framework\UrlInterface $urlBuilder)
    {
        $this->urlBuilder = $urlBuilder;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                // here we can also use the data from $item to configure some parameters of an action URL
                $item[$this->getData('name')] = [
                    'remove' => [
                        'href' => $this->urlBuilder->getUrl(self::DELETE_PATH, ['store_id' => $item['store_id']]),
                        'label' => __('Remove')
                    ],
                    'edit' => [
                        'href' => $this->urlBuilder->getUrl(self::EDIT_PATH, ['store_id' => $item['store_id']]),
                        'label' => __('Edit')
                    ],
                ];
            }
        }

        return $dataSource;
    }
}