<?php
declare(strict_types=1);

namespace Course\StoreLocator\Model;

use Course\StoreLocator\Api\Data\StoreInterface;
use Course\StoreLocator\Api\Data\StoreInterfaceFactory;
use Course\StoreLocator\Api\StoreRepositoryInterface;
use Course\StoreLocator\Model\ResourceModel\Store;
use Course\StoreLocator\Model\ResourceModel\Store\Collection;
use Course\StoreLocator\Model\ResourceModel\Store\CollectionFactory;
use Laminas\EventManager\EventManager;

class StoreRepository implements StoreRepositoryInterface
{

    private ResourceModel\Store $storeResource;
    private ResourceModel\StoreFactory $storeResourceFactory;
    private StoreInterfaceFactory $storeFactory;
    private CollectionFactory $storeCollectionFactory;
    private \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig;


    /**
     * @param Store $storeResource
     * @param StoreInterfaceFactory $storeFactory
     * @param CollectionFactory $storeCollectionFactory
     */
    public function __construct(
        \Course\StoreLocator\Model\ResourceModel\Store $storeResource,
        StoreInterfaceFactory $storeFactory,
        CollectionFactory $storeCollectionFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig

    )
    {
        $this->storeFactory = $storeFactory;
        $this->storeCollectionFactory = $storeCollectionFactory;

        $this->scopeConfig = $scopeConfig;
        $this->storeResource = $storeResource;
    }

    /**
     * @param StoreInterface $store
     * @return StoreInterface
     */
    public function save(StoreInterface $store)
    {
        try
        {
            $this->storeResource->save($store);
        }
        catch(\Exception $exception)
        {
            throw new \Magento\Framework\Exception\CouldNotSaveException('Can`t save');
        }
        return $store;
    }

    /**
     * @param int $store_id
     * @return StoreInterface
     */
    public function getById(int $store_id)
    {
        $store = $this->storeFactory->create();
        $this->storeResource->load($store, $store_id);
        if(!$store->getId())
        {
            throw new \Magento\Framework\Exception\NoSuchEntityException('cant load');
        }
        return $store;
    }

    /**
     * @param StoreInterface $footballer
     * @return StoreInterface
     */
    public function delete(StoreInterface $store)
    {
        try
        {
            $this->storeResource->delete($store);
        }
        catch (\Exception $exception)
        {
            throw new \Magento\Framework\Exception\CouldNotDeleteException('Can`t delete');
        }
    }

    /**
     * @param int $store_id
     * @return StoreInterface
     */
    public function deleteById(int $store_id)
    {
        return $this->delete($this->getById($store_id));
    }

    /**
     * @param
     * @return Collection
     */
    public function getList()
    {
        $collection = $this->storeCollectionFactory->create();
        return $collection->toArray();
    }
}
