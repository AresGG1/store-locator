<?php

declare(strict_types=1);

namespace Course\StoreLocator\Model;

use Course\StoreLocator\Api\Data\StoreInterface;
use Magento\Framework\Model\AbstractModel;

class Store extends AbstractModel implements StoreInterface
{
    public function _construct()
    {
        $this->_init(\Course\StoreLocator\Model\ResourceModel\Store::class);
    }

    /**
     * Identifier getter
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->getData(self::STORE_ID);
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->getData(self::NAME);
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->getData(self::DESCRIPTION);
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->getData(self::COUNTRY);
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->getData(self::CITY);
    }

    /**
     * @return mixed
     */
    public function getStreet()
    {
        return $this->getData(self::STREET);
    }

    /**
     * @return mixed
     */
    public function getZip()
    {
        return $this->getData(self::ZIP);
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->getData(self::IMAGE);
    }

    /**
     * @return mixed
     */
    public function getOpenTime()
    {
        return $this->getData(self::OPEN_TIME);
    }

    /**
     * @return mixed
     */
    public function getCloseTime()
    {
        return $this->getData(self::CLOSE_TIME);
    }

    /**
     * @return mixed
     */
    public function getLatitude()
    {
        return $this->getData(self::LATITUDE);
    }

    /**
     * @return mixed
     */
    public function getLongitude()
    {
        return $this->getData(self::LONGITUDE);
    }

    /**
     * @return mixed
     */
    public function getUpdateTime()
    {
        return $this->getData(self::UPDATE_TIME);
    }

    /**
     * @return mixed
     */
    public function getCreationTime()
    {
        return $this->getData(self::CREATION_TIME);
    }

    /**
     * @return mixed
     */
    public function getUrlKey()
    {
        return $this->getData(self::URL_KEY);
    }


    /**
     * @param mixed $value
     * @return mixed
     */
    public function setId($value)
    {
        $this->setData(self::STORE_ID, $value);
        return $this;
    }

    /**
     * @param $name
     * @return mixed
     */
    public function setName($name)
    {
        $this->setData(self::NAME, $name);
        return $this;
    }

    /**
     * @param $description
     * @return mixed
     */
    public function setDescription($description)
    {
        $this->setData(self::DESCRIPTION, $description);
        return $this;
    }

    /**
     * @param $country
     * @return mixed
     */
    public function setCountry($country)
    {
        $this->setData(self::COUNTRY, $country);
        return $this;
    }

    /**
     * @param $city
     * @return mixed
     */
    public function setCity($city)
    {
        $this->setData(self::CITY, $city);
        return $this;
    }

    /**
     * @param $street
     * @return mixed
     */
    public function setStreet($street)
    {
        $this->setData(self::STREET, $street);
        return $this;
    }

    /**
     * @param $zip
     * @return mixed
     */
    public function setZip($zip)
    {
        $this->setData(self::ZIP, $zip);
        return $this;
    }

    /**
     * @param $image
     * @return mixed
     */
    public function setImage($image)
    {
        $this->setData(self::IMAGE, $image);
        return $this;
    }

    /**
     * @param $open_time
     * @return mixed
     */
    public function setOpenTime($open_time)
    {
        $this->setData(self::OPEN_TIME, $open_time);
        return $this;
    }

    /**
     * @param $close_time
     * @return mixed
     */
    public function setCloseTime($close_time)
    {
        $this->setData(self::CLOSE_TIME, $close_time);
        return $this;
    }

    /**
     * @param $latitude
     * @return mixed
     */
    public function setLatitude($latitude)
    {
        $this->setData(self::LATITUDE, $latitude);
        return $this;
    }

    /**
     * @param $longitude
     * @return mixed
     */
    public function setLongitude($longitude)
    {
        $this->setData(self::LONGITUDE, $longitude);
        return $this;
    }

    /**
     * @param $creationTime
     * @return mixed
     */
    public function setCreationTime($creationTime)
    {
        $this->setData(self::CREATION_TIME, $creationTime);
        return $this;
    }

    /**
     * @param $updateTime
     * @return mixed
     */
    public function setUpdateTime($updateTime)
    {
        $this->setData(self::UPDATE_TIME, $updateTime);
        return $this;
    }
    /**
     * @param $urlKey
     * @return mixed
     */
    public function setUrlKey($urlKey)
    {
        $this->setData(self::URL_KEY, $urlKey);
        return $this;
    }
}
