<?php

declare(strict_types=1);

namespace Course\StoreLocator\Model\ResourceModel\Store;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @return void
     */
    public function _construct()
    {
        $this->_init(\Course\StoreLocator\Model\Store::class, \Course\StoreLocator\Model\ResourceModel\Store::class);
    }
}
