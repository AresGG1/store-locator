<?php

declare(strict_types=1);

namespace Course\StoreLocator\Model\ResourceModel;

use Course\StoreLocator\Api\Data\StoreInterface;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Store extends AbstractDb
{

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('course_storeLocator', StoreInterface::STORE_ID);
    }
}
