<?php

declare(strict_types=1);

namespace Course\StoreLocator\Controller\Obs;

use Course\StoreLocator\Model\Store;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\EntityManager\EventManager;

class Test implements HttpGetActionInterface
{
    private Store $store;
    private \Magento\Framework\Event\ManagerInterface $eventManager;

    public function __construct(
        Store $store,
        \Magento\Framework\Event\ManagerInterface $eventManager
    )
    {
        $this->store = $store;
        $this->eventManager = $eventManager;
    }

    public function execute()
    {
        echo 'loasah';
        $store = $this->store;
        $store->setName('loh');
        $this->eventManager->dispatch('course_storeLocator_save_before_observer', ['store'=>$store]);
    }
}
