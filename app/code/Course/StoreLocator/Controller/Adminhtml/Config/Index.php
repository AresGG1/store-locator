<?php

declare(strict_types=1);

namespace Course\StoreLocator\Controller\Adminhtml\Config;

use Magento\Backend\App\Action\Context;
use Magento\Framework\App\ResponseInterface;

class Index extends \Magento\Backend\App\Action
{

    private \Magento\Framework\View\Result\PageFactory $resultPageFactory;

    public function __construct(Context                                    $context,
                                \Magento\Framework\View\Result\PageFactory $resultPageFactory
)
    {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function execute()
    {
        $resultPage = $this->resultRedirectFactory->create();
        $resultPage->getConfig()->getTitle()->prepend((__('Store locator config')));
        return $resultPage;
    }
}
