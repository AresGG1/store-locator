<?php
declare(strict_types=1);

namespace Course\StoreLocator\Controller\Adminhtml\Index;

use Course\StoreLocator\Model\Store;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\ResponseInterface;


class Delete extends \Magento\Backend\App\Action implements HttpGetActionInterface
{


    private Store $store;

    public function __construct(Context $context,
                                Store   $store)
    {

        parent::__construct($context);
        $this->resultRedirect = $this->resultRedirectFactory->create();
        $this->store = $store;
    }

    public function execute()
    {
        $id = $this->getRequest()->getParam('store_id');
        try
        {
            $model = $this->store->load($id);
            $model->delete();
            $this->messageManager->addSuccessMessage(__('Store has been deleted.'));
        }
        catch (\Exception $exception)
        {
            $this->messageManager->addErrorMessage('Failed to delete');
        }
        return $this->resultRedirect->setPath('*/*/');
    }
}