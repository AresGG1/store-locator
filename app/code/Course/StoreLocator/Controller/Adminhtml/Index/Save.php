<?php
declare(strict_types=1);

namespace Course\StoreLocator\Controller\Adminhtml\Index;

use Course\StoreLocator\Model\StoreRepositoryFactory;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Filesystem;
use Magento\Framework\Validation\ValidationException;
use Magento\MediaStorage\Model\File\UploaderFactory;

class Save extends \Magento\Backend\App\Action {
    /**
     *
     * @var UploaderFactory
     */
    protected $uploaderFactory;

    /**
     * @var \Course\StoreLocator\Model\StoreFactory
     */
    protected $storeFactory;

    /**
     * @var Filesystem\Directory\WriteInterface
     */
    protected $mediaDirectory;
    private StoreRepositoryFactory $repositoryFactory;

    public function __construct(
        Context $context,
        UploaderFactory $uploaderFactory,
        Filesystem $filesystem,
        \Course\StoreLocator\Model\StoreFactory $storeFactory,
        StoreRepositoryFactory $repositoryFactory
    )
    {
        parent::__construct($context);
        $this->uploaderFactory = $uploaderFactory;
        $this->storeFactory = $storeFactory;
        $this->mediaDirectory = $filesystem->getDirectoryWrite(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
        $this->repositoryFactory = $repositoryFactory;
    }

    public function execute()
    {
        try {
            if ($this->getRequest()->getMethod() !== 'POST' || !$this->_formKeyValidator->validate($this->getRequest())) {
                throw new LocalizedException(__('Invalid Request'));
            }
            //validate image
            $fileUploader = null;
            $params = $this->getRequest()->getParams();
            $repository = $this->repositoryFactory->create();

            $model = $params['store_id']?$repository->getById((int)$params['store_id']):$this->storeFactory->create();


            if($params['image'][0]['name'] !== $model->getImage()) {
                try {
                    $imageId = 'image';
                    if (isset($params['image']) && count($params['image'])) {
                        $imageId = $params['image'][0];
                        if (!file_exists($imageId['tmp_name'])) {
                            $imageId['tmp_name'] = $imageId['path'] . '/' . $imageId['file'];
                        }
                    }
                    if ($params['image'] !== "") {
                        $fileUploader = $this->uploaderFactory->create(['fileId' => $imageId]);
                        $fileUploader->setAllowedExtensions(['jpg', 'jpeg', 'png']);
                        $fileUploader->setAllowRenameFiles(true);
                        $fileUploader->setAllowCreateFolders(true);
                        $fileUploader->validateFile();
                        //upload image
                        $info = $fileUploader->save($this->mediaDirectory->getAbsolutePath('imageUploader/images'));
                        $model->setImage($this->mediaDirectory->getRelativePath('imageUploader/images') . '/' . $info['file']);

                    }
                    /** @var \Course\StoreLocator\Model\StoreRepository */
                    /** @var \Course\StoreLocator\Model\Store */

                } catch (ValidationException $e) {
                    throw new LocalizedException(__('Image extension is not supported. Only extensions allowed are jpg, jpeg and png'));
                } catch (\Exception $e) {
                    //if an except is thrown, no image has been uploaded
                    throw new LocalizedException(__('Image is required'));
                }
            }
            $model->setName($params['name']);
            $model->setDescription($params['description']);
            $model->setCountry($params['country']);
            $model->setCity($params['city']);
            $model->setStreet($params['street']);
            $model->setZip($params['zip']);
            $model->setCloseTime($params['close_time']);
            $model->setOpenTime($params['open_time']);
            $model->setLongitude($params['longitude']);
            $model->setLatitude($params['latitude']);
            $model->setUrlKey($params['url_key']);
            $repository->save($model);
            return $this->_redirect('*/*/index');

            $this->messageManager->addSuccessMessage(__('Image uploaded successfully'));

        } catch (LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            return $this->_redirect('*/*/upload');
        } catch (\Exception $e) {
            error_log($e->getMessage());
            error_log($e->getTraceAsString());
            $this->messageManager->addErrorMessage(__('An error occurred, please try again later.'));
            return $this->_redirect('*/*/upload');
        }
    }
}