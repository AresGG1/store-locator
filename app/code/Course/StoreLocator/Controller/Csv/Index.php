<?php

declare(strict_types=1);

namespace Course\StoreLocator\Controller\Csv;

use Course\StoreLocator\Model\StoreFactory;
use Course\StoreLocator\Model\StoreRepository;

class Index implements \Magento\Framework\App\Action\HttpGetActionInterface
{

    private StoreRepository $storeRepository;
    private StoreFactory $storeFactory;

    public function __construct(
        StoreRepository $storeRepository,
        StoreFactory $storeFactory
    )
    {
        $this->storeRepository = $storeRepository;
        $this->storeFactory = $storeFactory;
    }

    public function execute()
    {
//        $array = [];
        $filename = "/home/ares/projects/1location.csv";
//        $open = fopen($filename, "r");
//        while(($data = fgetcsv($open, 1000, ",")) !== FALSE)
//        {
//            $array[] = $data;
//        }
//        foreach()
//        fclose($open);
        $fileData = file_get_contents($filename);
        $dataArray1 = str_getcsv($fileData, "\n");
        $dataArray2 = [];
        foreach ($dataArray1 as $row) {
            $dataArray2[] = explode(",", $row);
        }
        $dataArray = [];
        $count = 0;

        for($i=0, $iMax = count($dataArray2[0]); $i < $iMax; $i++)
        {
            if($i == 11)
                continue;
            $dataArray[0][] = $dataArray2[0][$i];
        }
        for ($j = 1, $jMax = count($dataArray2); $j < $jMax; $j++)
        {
            for ($i = 0, $iMax = count($dataArray2[1]); $i < $iMax; $i++) {
                if (($i > 10 and $i < 17) or $i == 18 or $i == 20)
                    continue;
                $dataArray[$j][$count] = $dataArray2[$j][$i];
                $count++;
            }
            $count=0;
        }

        $countRows = count($dataArray);
        $countColumns = count($dataArray[1]);
        $newArray = [];
        for ($i = 1; $i < $countRows; $i++) {
            for ($j = 0; $j < $countColumns; $j++) {
                $newArray[$i][$dataArray[0][$j]] = $dataArray[$i][$j];
            }
        }

        $repository = $this->storeRepository;
        foreach ($newArray as $item)
        {
            if($count == 20)
                break;
            $model = $this->storeFactory->create();
//            $model->setId($item['id']);
            $model->setName($item['name']);
            $model->setCountry($item['country']);
            $model->setCity($item['city']);
            $model->setStreet($item['address']);
            $model->setDescription($item['description']);
            $model->setImage($item['store_img']);
            $model->setOpenTime($item['schedule']);
            $model->setCloseTime($item['schedule']);
            $model->setUrlKey($item['url_key']);
            $model->setLongitude($item['position']);
            $model->setLatitude($item['position']);
            $count++;
            $repository->save($model);
            echo $count;
            if($count == 10)
                break;
            $count++;
        }
        exit;
    }
}
