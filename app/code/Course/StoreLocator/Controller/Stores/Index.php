<?php

declare(strict_types=1);

namespace Course\StoreLocator\Controller\Stores;

use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\ResponseInterface;

class Index extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory)
    {
        $this->_pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        $store = $this->_request->getParam('store');
        $page = $this->_pageFactory->create();
        $page->getConfig()->getTitle()->prepend($store->getName());
//        $page->getConfig()->
        return $page;
    }
}