<?php

declare(strict_types=1);

namespace Course\StoreLocator\Controller;

use Course\StoreLocator\Model\ResourceModel\Store\CollectionFactory;
use Course\StoreLocator\Model\Store;
use Course\StoreLocator\Model\StoreRepository;
use Course\StoreLocator\Model\StoreRepositoryFactory;
use Magento\Framework\App\Action\Forward;
use Magento\Framework\App\ActionFactory;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\RouterInterface;

/**
 * Class Router
 */
class Router implements RouterInterface
{
    /**
     * @var ActionFactory
     */
    private $actionFactory;

    /**
     * @var ResponseInterface
     */
    private $response;
    private CollectionFactory $collectionFactory;
    private StoreRepositoryFactory $storeRepositoryFactory;

    /**
     * Router constructor.
     *
     * @param ActionFactory $actionFactory
     * @param ResponseInterface $response
     */
    public function __construct(
        ActionFactory $actionFactory,
        ResponseInterface $response,
        CollectionFactory $collectionFactory,
        StoreRepositoryFactory $storeRepositoryFactory
    ) {
        $this->actionFactory = $actionFactory;
        $this->response = $response;
        $this->collectionFactory = $collectionFactory;
        $this->storeRepositoryFactory = $storeRepositoryFactory;
    }

    /**
     * @param RequestInterface $request
     * @return ActionInterface|null
     */
    public function match(\Magento\Framework\App\RequestInterface $request)
    {
        $urlKeys = [];
        $collection = $this->collectionFactory->create();
        $identifier = trim($request->getPathInfo(), '/');
        $url_key = substr($identifier,strpos($identifier, '/')+1);
        $id = null;
        /** @var Store $item */
        foreach ($collection as $item)
        {
            if ($url_key == $item->getUrlKey())
                $id = $item->getId();
        }
        $storeRepository = $this->storeRepositoryFactory->create();
        if($id) {
            $store = $storeRepository->getById((int)$id);
            $request->setModuleName('stores')->setControllerName('stores')->setActionName('index'); //action name
            $request->setParams(['store'=>$store]);
            return $this->actionFactory->create(
                Forward::class);
        }
    return null;
    }
}