<?php

declare(strict_types=1);

namespace Course\StoreLocator\Console\Command;

use Course\StoreLocator\Model\StoreFactory;
use Course\StoreLocator\Model\StoreRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ImportCommand extends Command
{

    private StoreFactory $storeFactory;
    private StoreRepository $storeRepository;

    public function __construct(StoreFactory $storeFactory, StoreRepository $storeRepository)
    {
        parent::__construct();
        $this->storeFactory = $storeFactory;
        $this->storeRepository = $storeRepository;
    }

    protected function configure(): void
    {
        $this->setName('import');
        $this->setDescription('This is my first console command.');
        $this->addOption(
            'path', null,             InputOption::VALUE_REQUIRED, 'Path'
        );
        parent::configure();
    }
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $filename = $input->getOption('path');
        $fileData = file_get_contents($filename);
        $dataArray1 = str_getcsv($fileData, "\n");
        $dataArray2 = [];
        foreach ($dataArray1 as $row) {
            $dataArray2[] = explode(",", $row);
        }
        $dataArray = [];
        $count = 0;

        for($i=0, $iMax = count($dataArray2[0]); $i < $iMax; $i++)
        {
            if($i == 11)
                continue;
            $dataArray[0][] = $dataArray2[0][$i];
        }
        for ($j = 1, $jMax = count($dataArray2); $j < $jMax; $j++)
        {
            for ($i = 0, $iMax = count($dataArray2[1]); $i < $iMax; $i++) {
                if (($i > 10 and $i < 17) or $i == 18 or $i == 20)
                    continue;
                $dataArray[$j][$count] = $dataArray2[$j][$i];
                $count++;
            }
            $count=0;
        }

        $countRows = count($dataArray);
        $countColumns = count($dataArray[1]);
        $newArray = [];
        for ($i = 1; $i < $countRows; $i++) {
            for ($j = 0; $j < $countColumns; $j++) {
                $newArray[$i][$dataArray[0][$j]] = $dataArray[$i][$j];
            }
        }
        $count = 0;
        $repository = $this->storeRepository;

        foreach ($newArray as $item)
        {
            $model = $this->storeFactory->create();
            $model->setName($item['name']);
            $model->setCountry($item['country']);
            $model->setCity($item['city']);
            $model->setStreet($item['address']);
            $model->setDescription($item['description']);
            $model->setImage($item['store_img']);
            $model->setOpenTime($item['schedule']);
            $model->setCloseTime($item['schedule']);
            $model->setUrlKey($item['url_key']);
            $model->setLongitude($item['position']);
            $model->setLatitude($item['position']);
            $repository->save($model);
            unset($model);
            $count++;
//            if($count == 2)
//                break;
        }
        $output->writeln('<info>Success</info>');
    }
}