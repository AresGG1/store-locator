<?php

declare(strict_types=1);

namespace Course\StoreLocator\Plugin;

use Course\StoreLocator\Model\Store;

class BeforeSave
{
    private \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    )
    {
        $this->scopeConfig = $scopeConfig;
    }

//    const API = "AIzaSyDIXvHvGS2Q9pNUu0vesfdBTR1y6sIngDU";

    public function beforeSave(\Course\StoreLocator\Model\ResourceModel\Store $storeResource ,Store $store)
    {
        if($store->getImage() === '' or $store->getImage() == null)
            $store->setImage('imageUploader/images/default.png');
        $latitude = $store->getLatitude();
        $longitude = $store->getLongitude();
        $address = $store->getCountry() .','.$store->getCity().','.$store->getStreet();
        $address = str_replace(' ', '', $address);
//        $store->setLatitude(11);
        if($latitude === "" or $longitude === "" or $latitude == "" or $longitude == "" or  $latitude == null or $longitude == null) {
            $api = $this->scopeConfig->getValue('storelocator/general/api');
            $url = "https://maps.googleapis.com/maps/api/geocode/json?key=" . $api . '&address=' . $address;
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_HEADER, false);
            $res = (string)curl_exec($ch);
            curl_close($ch);
            $array = json_decode($res, true);

            if ($array['status'] === 'OK') {
                $lat = $array["results"][0]["geometry"]["location"]['lat'];
                $lng = $array["results"][0]["geometry"]["location"]['lng'];
                if ($latitude === "" or $latitude == "" or $latitude == null)
                    $store->setLatitude((float)$lat);
                if ($longitude === "" or $longitude == "" or $longitude == null)
                    $store->setLongitude($lng);

            }

        }
    }
}

