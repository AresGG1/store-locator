<?php

declare(strict_types=1);

namespace Course\StoreLocator\Plugin;

use Course\StoreLocator\Model\ResourceModel\Store\CollectionFactory;
use Course\StoreLocator\Model\Store;

class BeforeSaveUrlKey
{
    private \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig;
    private CollectionFactory $collectionFactory;

    public function __construct(
        CollectionFactory $collectionFactory
    )
    {
        $this->collectionFactory = $collectionFactory;
    }


    public function beforeSave(\Course\StoreLocator\Model\ResourceModel\Store $storeResource, Store $store)
    {
        $url_key = $store->getUrlKey();
        $keys = [];
        $collection = $this->collectionFactory->create();

        /** @var Store $item */
        foreach ($collection as $item) {
            $keys[] = $item->getUrlKey();
        }
        if ($url_key == "" or $url_key == null or in_array($url_key, $keys)) {
            $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $url_key = substr(str_shuffle($permitted_chars), 0, 5);
            while (true) {
                if (in_array($url_key, $keys))
                        $url_key . str_shuffle($permitted_chars)[0];
                else
                    break;
            }
        }
        $store->setUrlKey($url_key);
    }
}


