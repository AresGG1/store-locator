<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Course\StoreLocator\Api\Data;

/**
 * CMS block interface.
 * @api
 * @since 100.0.2
 */
interface StoreInterface
{
    /**#@+
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const STORE_ID      = 'store_id';
    const NAME         = 'name';
    const DESCRIPTION       = 'description';
    const COUNTRY = 'country';
    const CITY = 'city';
    const STREET = 'street';
    const ZIP = 'zip';
    const IMAGE = 'image';
    const OPEN_TIME = 'open_time';
    const CLOSE_TIME = 'close_time';
    const LATITUDE = 'latitude';
    const LONGITUDE = 'longitude';
    const CREATION_TIME = 'creation_time';
    const UPDATE_TIME   = 'update_time';
    const URL_KEY   = 'url_key';
    /**#@-*/

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Get name
     *
     * @return string|null
     */
    public function getName();

    /**
     * Get description
     *
     * @return string|null
     */
    public function getDescription();

    /**
     * Get country
     *
     * @return string|null
     */
    public function getCountry();

    /**
     * Get city
     *
     * @return string|null
     */
    public function getCity();

    /**
     * Get street
     *
     * @return string|null
     */
    public function getStreet();

    /**
     * Get zip
     *
     * @return string|null
     */
    public function getZip();

    /**
     * Get image
     *
     * @return string|null
     */
    public function getImage();

    /**
     * Get open_time
     *
     * @return string|null
     */
    public function getOpenTime();

    /**
     * Get close_time
     *
     * @return string|null
     */
    public function getCloseTime();

    /**
     * Get latitude
     *
     * @return string|null
     */

    public function getLatitude();

    /**
     * Get latitude
     *
     * @return string|null
     */

    public function getLongitude();

    /**
     * Get longitude
     *
     * @return string|null
     */

    public function getCreationTime();

    /**
     * Get update time
     *
     * @return string|null
     */
    public function getUpdateTime();


    /**
     * Get url_key
     *
     * @return string|null
     */
    public function getUrlKey();

    /**
     * Set ID
     *
     * @param mixed $value
     * @return StoreInterface
     */
    public function setId($value);

    /**
     * Set name
     *
     * @param string $name
     * @return StoreInterface
     */
    public function setName($name);

    /**
     * Set description
     *
     * @param string $description
     * @return StoreInterface
     */
    public function setDescription($description);

    /**
     * Set country
     *
     * @param string $country
     * @return StoreInterface
     */
    public function setCountry($country);

    /**
     * Set city
     *
     * @param string $city
     * @return StoreInterface
     */
    public function setCity($city);

    /**
     * Set street
     *
     * @param string $street
     * @return StoreInterface
     */
    public function setStreet($street);

    /**
     * Set zip
     *
     * @param string $zip
     * @return StoreInterface
     */
    public function setZip($zip);

    /**
     * Set image
     *
     * @param string $image
     * @return StoreInterface
     */
    public function setImage($image);

    /**
     * Set open_time
     *
     * @param string $open_time
     * @return StoreInterface
     */
    public function setOpenTime($open_time);

    /**
     * Set close_time
     *
     * @param string $close_time
     * @return StoreInterface
     */
    public function setCloseTime($close_time);

    /**
     * Set latitude
     *
     * @param string $latitude
     * @return StoreInterface
     */
    public function setLatitude($latitude);

    /**
     * Set latitude
     *
     * @param string $longitude
     * @return StoreInterface
     */
    public function setLongitude($longitude);

    /**
     * Set creation time
     *
     * @param string $creationTime
     * @return StoreInterface
     */
    public function setCreationTime($creationTime);

    /**
     * Set update time
     *
     * @param string $updateTime
     * @return StoreInterface
     */
    public function setUpdateTime($updateTime);


    /**
     * Set update time
     *
     * @param string $urlKey
     * @return StoreInterface
     */
    public function setUrlKey($urlKey);
}
