<?php
declare(strict_types=1);

namespace Course\StoreLocator\Api;

use Course\Author\Api\Data\AuthorInterface;
use Course\StoreLocator\Api\Data\StoreInterface;
use Course\StoreLocator\Model\ResourceModel\Store\Collection;
use Magento\Framework\Api\SearchCriteriaInterface;

interface StoreRepositoryInterface
{

    /**
     * POST for Store api
     * @param StoreInterface $store
     * @return StoreInterface
     */
    public function save(StoreInterface $store);

    /**
     * GET for Store api
     * @param int $store_id
     * @return StoreInterface
     */
    public function getById(int $store_id);

    /**
     * DELETE for Store api
     * @param StoreInterface $store
     * @return StoreInterface
     */
    public function delete(StoreInterface $store);

    /**
     * DELETE for Store api
     * @param int $store_id
     * @return StoreInterface
     */
    public function deleteById(int $store_id);

    /**
     * GET for Store api
     * @param SearchCriteriaInterface $criteria
     * @return Collection
     */
    public function getList();

}

