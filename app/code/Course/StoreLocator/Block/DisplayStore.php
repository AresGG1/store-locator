<?php
declare(strict_types=1);

namespace Course\StoreLocator\Block;

use Course\StoreLocator\Model\Store;
use Magento\Framework\View\Element\Template;
use Magento\Framework\App\RequestInterface;

class DisplayStore extends \Magento\Framework\View\Element\Template
{

    private RequestInterface $request;

    public function __construct(Template\Context $context, array $data = [], RequestInterface $request)
    {
        parent::__construct($context, $data);
        $this->request = $request;
    }

    public function getInfo()
    {
        /** @var Store $store */
        $store = $this->_request->getParam('store');
        $address = $store->getCountry().','.$store->getCity().','.$store->getStreet();
        return ['open_time' => $store->getOpenTime(), 'close_time'=>$store->getCloseTime(),'address'=>$address, 'desc'=>$store->getDescription()];
    }

}
