<?php

declare(strict_types=1);

namespace Course\StoreLocator\Block;

use Course\StoreLocator\Model\ResourceModel\Store\Collection;
use Course\StoreLocator\Model\ResourceModel\Store\CollectionFactory;
use Course\StoreLocator\Model\Store;
use Magento\Framework\UrlInterface;

class Display extends \Magento\Framework\View\Element\Template
{

    private CollectionFactory $collectionFactory;

    public function __construct(\Magento\Framework\View\Element\Template\Context $context,
                                CollectionFactory                                $collectionFactory)
    {
        parent::__construct($context);
        $this->collectionFactory = $collectionFactory;
    }

    public function showList()
    {
        $collection = $this->collectionFactory->create();

        /** @var Store $elem */
        foreach ($collection as $elem) {
            $url = $this->_urlBuilder->getCurrentUrl();
            $url = trim($url, '/');
            $key = $elem->getUrlKey();
            echo "<table>";
            echo "<tr>";
            echo '<th><a href="';
            echo $url.'/'.$key;
            echo "\">";
            echo $elem->getName()."</th>";
            echo "</tr>";
            echo "</table>";
        }
    }
}
