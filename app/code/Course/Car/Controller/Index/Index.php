<?php
declare(strict_types=1);

namespace Course\Car\Controller\Index;


use Course\Car\Api\Data\CarInterfaceFactory;
use Course\Car\Model\ResourceModel\Car\CollectionFactory;
use Magento\Framework\App\Action\HttpGetActionInterface;


class Index implements HttpGetActionInterface
{
    private CarInterfaceFactory $carFactory;
    private CollectionFactory $collectionFactory;

    public function __construct(
        CarInterfaceFactory $carFactory,
        CollectionFactory $collectionFactory
    )
    {
        $this->carFactory = $carFactory;
        $this->collectionFactory = $collectionFactory;
    }

    public function execute()
    {
        $collection = $this->collectionFactory->create();
        foreach ($collection as $car)
        {
            echo $car->getName()." ";
        }
    }
}