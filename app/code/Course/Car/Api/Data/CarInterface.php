<?php


declare(strict_types=1);

namespace Course\Car\Api\Data;

interface CarInterface
{

    const CAR_ID = 'car_id';
    const NAME = 'name';
    const INFO = 'info';
    const CREATION_TIME = 'creation_time';
    const UPDATE_TIME = 'update_time';

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Get title
     *
     * @return string|null
     */
    public function getName(): ?string;

    /**
     * Get content
     *
     * @return string|null
     */
    public function getInfo(): ?string;

    /**
     * Get creation time
     *
     * @return string|null
     */
    public function getCreationTime(): ?string;

    /**
     * Get update time
     *
     * @return string|null
     */
    public function getUpdateTime(): ?string;

    /**
     * Set ID
     *
     * @param int $value
     * @return CarInterface
     */
    public function setId(int $value): CarInterface;

    /**
     * Set title
     *
     * @param string $name
     * @return CarInterface
     */
    public function setName(string $name): CarInterface;

    /**
     * Set content
     *
     * @param string $info
     * @return CarInterface
     */
    public function setInfo(string $info): CarInterface;

    /**
     * Set creation time
     *
     * @param string $creationTime
     * @return CarInterface
     */
    public function setCreationTime(string $creationTime): CarInterface;

    /**
     * Set update time
     *
     * @param string $updateTime
     * @return CarInterface
     */
    public function setUpdateTime(string $updateTime): CarInterface;

}

