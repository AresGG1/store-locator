<?php

declare(strict_types=1);

namespace Course\Car\Model;

use Course\Car\Api\Data\CarInterface;
use Magento\Framework\Model\AbstractModel;

class Car extends AbstractModel implements CarInterface
{
    public function _construct()
    {
        $this->_init(\Course\Car\Model\ResourceModel\Car::class);
    }

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->getData(self::CAR_ID);
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->getData(self::NAME);
    }

    /**
     * @return string|null
     */
    public function getInfo(): ?string
    {
        return $this->getData(self::INFO);
    }

    /**
     * @return string|null
     */
    public function getCreationTime(): ?string
    {
        return $this->getData(self::CREATION_TIME);
    }

    /**
     * @return string|null
     */
    public function getUpdateTime(): ?string
    {
        return $this->getData(self::UPDATE_TIME);
    }

    public function setId($value):CarInterface
    {
        return $this->setData(self::CAR_ID, $value);
    }


    /**
     * @param string $name
     * @return CarInterface
     */
    public function setName(string $name): CarInterface
    {
        $this->setData(self::NAME, $name);
        return $this;
    }

    /**
     * @param string $info
     * @return CarInterface
     */
    public function setInfo(string $info): CarInterface
    {
        $this->setData(self::INFO, $info);
        return $this;
    }

    /**
     * @param string $creationTime
     * @return CarInterface
     */
    public function setCreationTime(string $creationTime): CarInterface
    {
        $this->setData(self::CREATION_TIME, $creationTime);
        return $this;
    }

    /**
     * @param string $updateTime
     * @return CarInterface
     */
    public function setUpdateTime(string $updateTime): CarInterface
    {
        $this->setData(self::UPDATE_TIME, $updateTime);
        return $this;
    }
}
