<?php


declare(strict_types=1);

namespace Course\Car\Model\ResourceModel\Car;

use Course\Car\Model\Car;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
            Car::class,\Course\Car\Model\ResourceModel\Car::class
        );
    }
}