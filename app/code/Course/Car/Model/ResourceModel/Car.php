<?php

declare(strict_types=1);

namespace Course\Car\Model\ResourceModel;


use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Car extends AbstractDb
{
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('course_car', 'car_id');
    }
}
