<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Course\Football\Api\Data;

/**
 * CMS block interface.
 * @api
 * @since 100.0.2
 */
interface FootballerInterface
{
    /**#@+
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const ID            = 'id';
    const NAME          = 'name';
    const CLUB          = 'club';
    const CREATION_TIME = 'creation_time';
    const UPDATE_TIME   = 'update_time';
    /**#@-*/

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Get name
     *
     * @return string|null
     */
    public function getName();

    /**
     * Get club
     *
     * @return string|null
     */
    public function getClub();

    /**
     * Get creation time
     *
     * @return string|null
     */
    public function getCreationTime();

    /**
     * Get update time
     *
     * @return string|null
     */
    public function getUpdateTime();


    /**
     * Set ID
     *
     * @param int $value
     * @return FootballerInterface
     */
    public function setId($value);

    /**
     * Set name
     *
     * @param string $name
     * @return FootballerInterface
     */
    public function setName($name);

    /**
     * Set content
     *
     * @param string $club
     * @return FootballerInterface
     */
    public function setClub($club);

    /**
     * Set creation time
     *
     * @param string $creationTime
     * @return FootballerInterface
     */
    public function setCreationTime($creationTime);

    /**
     * Set update time
     *
     * @param string $updateTime
     * @return FootballerInterface
     */
    public function setUpdateTime($updateTime);

}
