<?php
declare(strict_types=1);

namespace Course\Football\Api;


use Course\Author\Api\Data\AuthorInterface;
use Course\Football\Api\Data\FootballerInterface;

interface FootballerRepositoryInterface
{

    public function save(FootballerInterface $footballer);

    public function getById(int $id);

    public function delete(FootballerInterface $footballer);

    public function deleteById(int $id);

    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $criteria);


}

