<?php

declare(strict_types=1);

namespace Course\Football\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Footballer extends AbstractDb
{

    protected function _construct()
    {
        $this->_init("course_football", 'id');
    }
}
