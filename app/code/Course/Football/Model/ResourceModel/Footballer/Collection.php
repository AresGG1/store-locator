<?php

namespace Course\Football\Model\ResourceModel\Footballer;


use Course\Football\Model\Footballer;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    public function _construct()
    {
        $this->_init(Footballer::class, \Course\Football\Model\ResourceModel\Footballer::class);
    }
}