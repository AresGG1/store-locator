<?php

declare(strict_types=1);

namespace Course\Football\Model;

use Course\Football\Api\Data\FootballerInterface;
use Magento\Framework\Model\AbstractModel;

class Footballer extends AbstractModel implements FootballerInterface
{
    public function _construct()
    {
        $this->_init(\Course\Football\Model\ResourceModel\Footballer::class);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->getData(self::ID);
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->getData(self::NAME);
    }

    /**
     * @return mixed
     */
    public function getClub()
    {
        return $this->getData(self::CLUB);
    }

    /**
     * @return mixed
     */
    public function getCreationTime()
    {
        return $this->getData(self::CREATION_TIME);
    }

    /**
     * @return mixed
     */
    public function getUpdateTime()
    {
        return $this->getData(self::UPDATE_TIME);
    }

    /**
     * @param $value
     * @return mixed
     */
    public function setId($value)
    {
        $this->setData(self::ID, $value);
        return $this;
    }

    /**
     * @param $name
     * @return mixed
     */
    public function setName($name)
    {
        $this->setData(self::NAME, $name);
        return $this;
    }

    /**
     * @param $club
     * @return mixed
     */
    public function setClub($club)
    {
        $this->setData(self::CLUB, $club);
        return $this;
    }

    /**
     * @param $creationTime
     * @return mixed
     */
    public function setCreationTime($creationTime)
    {
        $this->setData(self::CREATION_TIME, $creationTime);
        return $this;
    }

    /**
     * @param $updateTime
     * @return mixed
     */
    public function setUpdateTime($updateTime)
    {
        $this->setData(self::UPDATE_TIME, $updateTime);
        return $this;
    }
}
