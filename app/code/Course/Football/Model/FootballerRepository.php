<?php

declare(strict_types=1);

namespace Course\Football\Model;

use Course\Football\Api\Data\FootballerInterface;
use Course\Football\Api\FootballerRepositoryInterface;

class FootballerRepository implements FootballerRepositoryInterface
{

    private ResourceModel\Footballer $footballerResource;
    private \Course\Football\Api\Data\FootballerInterfaceFactory $footballerFactory;

    public function __construct(
        \Course\Football\Model\ResourceModel\Footballer $footballerResource,
        \Course\Football\Api\Data\FootballerInterfaceFactory $footballerFactory

    )
    {

        $this->footballerResource = $footballerResource;
        $this->footballerFactory = $footballerFactory;
    }


    /**
     * @param FootballerInterface $footballer
     * @return mixed
     */
    public function save(FootballerInterface $footballer)
    {
        try
        {
            $this->footballerResource->save($footballer);
        }
        catch(\Exception $exception)
        {
            throw new \Magento\Framework\Exception\CouldNotSaveException('Can`t save');
        }
        return $footballer;
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function getById(int $id)
    {
        $footballer = $this->footballerFactory->create();
        $this->footballerResource->load($footballer, $id);
        if(!$footballer->getId())
        {
            throw new \Magento\Framework\Exception\NoSuchEntityException('cant load');
        }
        return $footballer;
    }

    /**
     * @param FootballerInterface $footballer
     * @return mixed
     */
    public function delete(FootballerInterface $footballer)
    {
        try
        {
            $this->footballerResource->delete($footballer);
        }
        catch(Exception $ex)
        {
            throw new \Magento\Framework\Exception\CouldNotDeleteException('Can`t delete');
        }
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function deleteById(int $id)
    {
        return $this->delete($this->getById($id));
    }

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $criteria
     * @return mixed
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $criteria)
    {
        return null;
    }
}
