<?php

declare(strict_types=1);

namespace Course\Football\CustomRouter;
use Magento\Framework\App\Action\Forward;
use Magento\Framework\App\ActionFactory;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\RouterInterface;

/**
 * Class Router
 */
class Router implements RouterInterface
{
    /**
     * @var ActionFactory
     */
    private $actionFactory;

    /**
     * @var ResponseInterface
     */
    private $response;

    /**
     * Router constructor.
     *
     * @param ActionFactory $actionFactory
     * @param ResponseInterface $response
     */
    public function __construct(
        ActionFactory $actionFactory,
        ResponseInterface $response
    ) {

        $this->actionFactory = $actionFactory;
        $this->response = $response;
    }

    /**
     *
     * @param RequestInterface $request
     * @return ActionInterface|null
     */
    public function match(\Magento\Framework\App\RequestInterface $request)
    {
        $identifier = trim($request->getPathInfo(), '/');
        if (strpos($identifier, 'sho') !== false) {
            $request->setModuleName('football')->setControllerName('index')->setActionName('index'); //action name
        } else {
            return null;
        }
        return $this->actionFactory->create(
            Forward::class
        );
    }
}