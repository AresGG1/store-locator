<?php

declare(strict_types=1);

namespace Course\Football\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

/**
 * Class Actions
 */
class Actions extends Column
{
    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */

    private const EDIT_PATH = 'custommenu/create/edit/';
    private const DELETE_PATH = 'custommenu/create/delete/';
    private const DUPLICATE_PATH = 'custommenu/create/duplicate/';
    private \Magento\Framework\UrlInterface $urlBuilder;

    public function __construct(ContextInterface $context, UiComponentFactory $uiComponentFactory, array $components = [], array $data = [],
                                \Magento\Framework\UrlInterface $urlBuilder)
    {
        $this->urlBuilder = $urlBuilder;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                // here we can also use the data from $item to configure some parameters of an action URL
                $item[$this->getData('name')] = [
                    'remove' => [
                        'href' => $this->urlBuilder->getUrl(self::DELETE_PATH, ['id' => $item['id']]),
                        'label' => __('Remove')
                    ],
                    'duplicate' => [
                        'href' => $this->urlBuilder->getUrl(self::DUPLICATE_PATH, ['id' => $item['id']]),
                        'label' => __('Duplicate')
                    ],
                ];
            }
        }

        return $dataSource;
    }
}