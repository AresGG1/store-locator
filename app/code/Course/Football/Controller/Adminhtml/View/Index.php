<?php

declare(strict_types=1);

namespace Course\Football\Controller\Adminhtml\View;

use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\ResponseInterface;

class Index implements HttpGetActionInterface
{
    public function execute()
    {
        echo 'viewed';
        exit;
    }
}