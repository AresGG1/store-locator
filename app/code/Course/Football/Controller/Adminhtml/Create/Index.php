<?php

declare(strict_types=1);

namespace Course\Football\Controller\Adminhtml\Create;

use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\ResponseInterface;

class Index extends \Magento\Backend\App\Action
{
    public const ADMIN_RESOURCE = 'Course_Football::create';

    protected $resultPageFactory = false;

    public function __construct(
        \Magento\Backend\App\Action\Context        $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    )
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->prepend((__('Football')));
        return $resultPage;
    }
}
