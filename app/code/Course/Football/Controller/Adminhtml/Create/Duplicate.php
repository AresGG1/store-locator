<?php
declare(strict_types=1);

namespace Course\Football\Controller\Adminhtml\Create;

use Course\Football\Model\Footballer;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\ResponseInterface;


class Duplicate extends \Magento\Backend\App\Action implements HttpGetActionInterface
{



    private Footballer $footballer;

    public function __construct(Context    $context,
                                Footballer $footballer)
    {

        parent::__construct($context);
        $this->footballer = $footballer;
        $this->resultRedirect = $this->resultRedirectFactory->create();
    }

    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        try
        {
            $model = $this->footballer->load($id);
            $name = $model->getName();
            $club = $model->getClub();
            $newFootballer = $this->_objectManager->create(Footballer::class);
            $newFootballer->setName($name)->setClub($club)->save();
            $this->messageManager->addSuccessMessage(__('Duplication succes'));
        }
        catch (\Exception $exception)
        {
            $this->messageManager->addErrorMessage('Failed to duplicate');
        }
        return $this->resultRedirect->setPath('*/*/');
    }
}