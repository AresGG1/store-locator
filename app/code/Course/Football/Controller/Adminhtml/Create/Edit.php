<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Course\Football\Controller\Adminhtml\Create;

use Course\Football\Model\Footballer;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;

/**
 * Edit CMS page action.
 */
class Edit extends \Magento\Backend\App\Action implements HttpGetActionInterface
{
    private \Magento\Framework\View\Result\PageFactory $resultPageFactory;

    public function __construct(Context                                    $context,
                                \Magento\Framework\View\Result\PageFactory $resultPageFactory
)
    {

        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    public function execute()
    {
        $page = $this->resultPageFactory->create();
        $page->getConfig()->getTitle()->prepend('Editing store');
        return $page;
    }
}

