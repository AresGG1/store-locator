<?php
declare(strict_types=1);

namespace Course\Football\Controller\Adminhtml\Create;

use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;

class Save extends \Magento\Backend\App\Action implements \Magento\Framework\App\Action\HttpPostActionInterface
{
    /**
     * @var \Course\Football\Model\FootballerFactory $footballerRequestFactory
     */
    private \Course\Football\Model\FootballerFactory $footballerRequestFactory;

    /**
     * @var \Course\Football\Model\ResourceModel\Footballer $footballerResource
     */
    private \Course\Football\Model\ResourceModel\Footballer $footballerResource;

    /**
     * Save constructor.
     * @param \Course\Football\Model\FootballerFactory $footballerFactory
     * @param \Course\Football\Model\ResourceModel\Footballer $footballerResource
     * @param \Magento\Backend\App\Action\Context $context
     */
    public function __construct(
        \Course\Football\Model\FootballerFactory $footballerFactory,
        \Course\Football\Model\ResourceModel\Footballer $footballerResource,
        \Magento\Backend\App\Action\Context $context
    ) {
        parent::__construct($context);
        $this->footballerFactory = $footballerFactory;
        $this->footballerResource = $footballerResource;
    }


    /**
     * @return ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        try
        {
            $resultRedirect = $this->resultRedirectFactory->create();
            $request = $this->getRequest();
            $name = $request->getParam('name');
            $club = $request->getParam('club');
            $footballer = $this->footballerFactory->create();
            $footballer->setName($name)->setClub($club)->save();
            $this->messageManager->addSuccessMessage(__('Footballer has been added'));
            return $resultRedirect->setPath('*/*/');
        }
        catch (\Exception $exception)
        {
            $this->messageManager->addErrorMessage('Failed to add');
        }
    }
}
