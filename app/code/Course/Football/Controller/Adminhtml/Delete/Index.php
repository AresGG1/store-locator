<?php

declare(strict_types=1);

namespace Course\Football\Controller\Adminhtml\Delete;

use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\ResponseInterface;

class Index implements HttpGetActionInterface
{
    public function execute()
    {
        echo 'deleted';
        exit;
    }
}