<?php

declare(strict_types=1);

namespace Course\Football\Controller\Index;

use Course\Football\Api\Data\FootballerInterfaceFactory;
use Course\Football\Api\FootballerRepositoryInterface;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\ResponseInterface;

class Index implements HttpGetActionInterface
{
    private ResponseInterface $response;
    private FootballerInterfaceFactory $footballerFactory;
    private FootballerRepositoryInterface $footballerRepository;

    public function __construct(
        ResponseInterface $response,
        FootballerInterfaceFactory $footballerFactory,
        FootballerRepositoryInterface $footballerRepository
    )
    {
        $this->response = $response;
        $this->footballerFactory = $footballerFactory;
        $this->footballerRepository = $footballerRepository;
    }

    public function execute()
    {
        $footballer = $this->footballerFactory->create();
        $footballer->setName('asla')->setClub('Epam');
        $this->footballerRepository->save($footballer);
        $this->response->sendResponse();
        echo 'sajasj';
        exit;
    }
}