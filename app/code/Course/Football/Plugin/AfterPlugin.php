<?php

declare(strict_types=1);

namespace Course\Football\Plugin;

use Course\Author\Model\Author;
use Course\Football\Model\Footballer;

class AfterPlugin
{
    public function afterGetName(Footballer $footballer, $name)
    {
        return $name;
    }
}
