<?php

declare(strict_types=1);

namespace Course\FirstModule\Block;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\Template;

class Productinfo extends Template
{
    const PRODUCT_SKU = "WS10";
    public function __construct(
        ProductRepositoryInterface $productRepository,
        Template\Context $context, array $data = [])
    {
        parent::__construct($context, $data);
        $this->productRepository = $productRepository;
    }

    public function getProduct(): ?ProductInterface
    {
        try {
            return $this->productRepository->get(self::PRODUCT_SKU);
        }
        catch (NoSuchEntityException $ex)
        {
            echo __("not found");
        }
        return null;
    }
}
