<?php
declare(strict_types=1);

namespace Course\FirstModule\Controller\Index;


use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\Controller\Result\RawFactory;
use Magento\Framework\Controller\Result\ForwardFactory;

class NotIndex implements HttpGetActionInterface
{

    private RawFactory $rawFactory;
//    private ForwardFactory $forwardFactory;

    public function __construct(
        RawFactory $rawFactory
//        ForwardFactory $forwardFactory
    )
    {
        $this->rawFactory = $rawFactory;
//        $this->forwardFactory = $forwardFactory;
    }

    public function execute()
    {
        return $this->rawFactory->create()->setHeader('Content-Type','text/html')->setContents('<form action="../index" method="get"><input type="text" name="name"></input><button type="submit">submit</button></form>');
    }
}
