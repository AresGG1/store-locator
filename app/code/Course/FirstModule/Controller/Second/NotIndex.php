<?php

declare(strict_types=1);

namespace Course\FirstModule\Controller\Second;

use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\ResponseInterface;

class NotIndex implements HttpGetActionInterface
{

    public function __construct(
        ResponseInterface $response
    )
    {
        $this->response = $response;
    }


    public function execute()
    {
        echo "I am NOT index controller";
        $this->response->sendResponse();
    }
}
