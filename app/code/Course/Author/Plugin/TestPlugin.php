<?php
declare(strict_types=1);

namespace Course\Author\Plugin;

use Course\Author\Model\Author;

class TestPlugin
{
    public function beforeSetName(Author $author, $name)
    {
        return 'name' . '- loh';
    }
}
