<?php

declare(strict_types=1);

namespace Course\Author\Api\Data;

interface AuthorInterface
{

    const AUTHOR_ID = 'author_id';
    const NAME = 'name';
    const INFO = 'info';
    const CREATION_TIME = 'creation_time';
    const UPDATE_TIME = 'update_time';

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId():?int;

    /**
     * Get title
     *
     * @return string|null
     */
    public function getName():?string;

    /**
     * Get content
     *
     * @return string|null
     */
    public function getInfo():?string;

    /**
     * Get creation time
     *
     * @return string|null
     */
    public function getCreationTime():?string;

    /**
     * Get update time
     *
     * @return string|null
     */
    public function getUpdateTime():?string;

    /**
     * Set ID
     *
     * @param int $id
     * @return AuthorInterface
     */
    public function setId(int $value):AuthorInterface;

    /**
     * Set title
     *
     * @param string $name
     * @return AuthorInterface
     */
    public function setName(string $name):AuthorInterface;

    /**
     * Set content
     *
     * @param string $info
     * @return AuthorInterface
     */
    public function setInfo(string $info):AuthorInterface;

    /**
     * Set creation time
     *
     * @param string $creationTime
     * @return AuthorInterface
     */
    public function setCreationTime(string $creationTime):AuthorInterface;

    /**
     * Set update time
     *
     * @param string $updateTime
     * @return AuthorInterface
     */
    public function setUpdateTime(string $updateTime):AuthorInterface;

}

