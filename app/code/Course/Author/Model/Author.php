<?php
declare(strict_types=1);

namespace Course\Author\Model;
use Course\Author\Api\Data\AuthorInterface;
use Magento\Framework\Model\AbstractModel;


class Author extends AbstractModel implements AuthorInterface
{

    protected function _construct()
    {
        $this->_init(\Course\Author\Model\ResourceModel\Author::class);
    }

    public function getId():?int
    {
        return (int)$this->getData(self::AUTHOR_ID);
    }

    public function getName(): ?string
    {
        return (string)$this->getData(self::NAME);
    }

    public function getInfo(): ?string
    {
        return (string)$this->getData(self::INFO);
    }

    public function getCreationTime(): ?string
    {
        return $this->getData(self::CREATION_TIME);
    }

    public function getUpdateTime(): ?string
    {
        return $this->getData(self::UPDATE_TIME);
    }
    public function setId($value):AuthorInterface
    {
        return $this->setData(self::AUTHOR_ID, $value);
    }

    public function setName(string $name): AuthorInterface
    {
        return $this->setData(self::NAME, $name);
    }

    public function setInfo(string $info): AuthorInterface
    {
        return $this->setData(self::INFO, $info);
    }

    public function setCreationTime(string $creationTime): AuthorInterface
    {
        return $this->setData(self::CREATION_TIME, $creationTime);
    }

    public function setUpdateTime(string $updateTime): AuthorInterface
    {
        return $this->setData(self::UPDATE_TIME, $updateTime);
    }
}
