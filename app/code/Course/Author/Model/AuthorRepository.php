<?php

namespace Course\Author\Model;

class AuthorRepository implements \Course\Author\Api\AuthorRepositoryInterface
{

    public function __construct(

        \Course\Author\Model\ResourceModel\Author $authorResource,
                \Course\Author\Api\Data\AuthorInterfaceFactory $authorFactory

    )
    {
        $this->authorResource = $authorResource;
        $this->authorFactory = $authorFactory;
    }

    /**
     * @param \Course\Author\Api\Data\AuthorInterface $author
     * @return mixed
     */
    public function save(\Course\Author\Api\Data\AuthorInterface $author)
    {
        try
        {
            $this->authorResource->save($author);
        }
        catch(\Exception $exception)
        {
            throw new \Magento\Framework\Exception\CouldNotSaveException('Can`t save');
        }
        return $author;
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function getById(int $id)
    {
        $author = $this->authorFactory->create();
        $this->authorResource->load($author, $id);
        if(!$author->getId())
        {
            throw new \Magento\Framework\Exception\NoSuchEntityException('cant load');
        }
        return $author;
    }

    /**
     * @param \Course\Author\Api\Data\AuthorInterface $author
     * @return mixed
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     */
    public function delete(\Course\Author\Api\Data\AuthorInterface $author)
    {
        try
        {
            $this->authorResource->delete($author);
        }
        catch(Exception $ex)
        {
            throw new \Magento\Framework\Exception\CouldNotDeleteException('Can`t delete');
        }
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function deleteById(int $id)
    {
        return $this->delete($this->getById($id));
    }
    /**
     * Load Block data collection by given search criteria
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @param \Magento\Framework\Api\SearchCriteriaInterface $criteria
     * @return \Magento\Cms\Api\Data\BlockSearchResultsInterface
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $criteria)
    {
        /** @var \Magento\Cms\Model\ResourceModel\Block\Collection $collection */
        $collection = $this->blockCollectionFactory->create();

        $this->collectionProcessor->process($criteria, $collection);

        /** @var Data\BlockSearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }
}
