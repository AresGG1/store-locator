<?php
declare(strict_types=1);

namespace Course\Author\Model\ResourceModel\Author;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Course\Author\Model\Author;
use Course\Author\Model\ResourceModel\Author as AuthorResource;


class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init(Author::class,
            AuthorResource::class
    );
    }
}
