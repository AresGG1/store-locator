<?php
declare(strict_types=1);

namespace Course\Author\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class MyEvent implements ObserverInterface
{

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        echo 'hi, '.$observer->getData('name').' !';
        return $observer;
    }
}
