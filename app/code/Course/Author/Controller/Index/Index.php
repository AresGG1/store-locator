<?php
declare(strict_types=1);

namespace Course\Author\Controller\Index;

use Course\Author\Api\AuthorRepositoryInterface;
use Course\Author\Model\Author;
use Course\Author\Model\ResourceModel;
use Course\Author\Api\Data\AuthorInterface;
use Course\Author\Api\Data\AuthorInterfaceFactory;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Exception\AlreadyExistsException;

class Index implements HttpGetActionInterface
{
    private ResponseInterface $response;
    private AuthorInterfaceFactory $authorInterfaceFactory;
    private AuthorRepositoryInterface $authorRepository;
    private ResourceModel\Author\CollectionFactory $AuthorCollectionFactory;

    public function __construct(
        AuthorInterfaceFactory $authorInterfaceFactory,
        ResponseInterface $response,
        AuthorRepositoryInterface $authorRepository,
        ResourceModel\Author\CollectionFactory $AuthorCollectionFactory
    )
    {
        $this->response = $response;
        $this->authorInterfaceFactory = $authorInterfaceFactory;
        $this->authorRepository = $authorRepository;
        $this->AuthorCollectionFactory = $AuthorCollectionFactory;
    }

    public function execute()
    {
        echo 'loh';
        $author = $this->authorInterfaceFactory->create();
        $author->setName('Ivan Franko')->setInfo('1890');
        $this->authorRepository->save($author);
    }
}