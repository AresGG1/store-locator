<?php

declare(strict_types=1);

namespace Course\Author\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\ResponseInterface;

class Test extends \Magento\Framework\App\Action\Action
{

    public function execute()
    {
        echo 'sa';
        $this->_eventManager->dispatch('course_author_event_after', array('name'=>'Vova'));
    }
}
