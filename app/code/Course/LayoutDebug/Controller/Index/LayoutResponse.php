<?php
declare(strict_types=1);

namespace Course\LayoutDebug\Controller\Index;

class LayoutResponse implements \Magento\Framework\App\Action\HttpGetActionInterface
{

    private \Magento\Framework\View\Result\LayoutFactory $layoutFactory;

    public function __construct(
        \Magento\Framework\View\Result\LayoutFactory $layoutFactory
    )
    {
        $this->layoutFactory = $layoutFactory;
    }

    public function execute()
    {
        return $this->layoutFactory->create();
    }

}
