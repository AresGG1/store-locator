<?php
declare(strict_types=1);

namespace Course\LayoutDebug\Controller\Index;

use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\ResponseInterface;

class Index implements HttpGetActionInterface
{
    public function __construct(
        ResponseInterface $response
    )
    {
        $this->response = $response;
    }

    public function execute()
    {
        echo 'loh';
        $this->response->sendResponse();
    }
}
