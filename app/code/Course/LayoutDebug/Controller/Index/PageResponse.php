<?php
declare(strict_types=1);

namespace Course\LayoutDebug\Controller\Index;


class PageResponse implements \Magento\Framework\App\Action\HttpGetActionInterface
{

    private \Magento\Framework\View\Result\PageFactory $pageFactory;

    public function __construct(
        \Magento\Framework\View\Result\PageFactory $pageFactory
    )
    {
        $this->pageFactory = $pageFactory;
    }

    public function execute()
    {
        return $this->pageFactory->create();
    }
}
